package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world",translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() throws PigLatinException {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL,translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithAEndingWithY() throws PigLatinException {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithUEndingWithY() throws PigLatinException {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() throws PigLatinException {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() throws PigLatinException {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithConsonant() throws PigLatinException {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMultipleConsonants() throws PigLatinException {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseMultipleWordsSeparatedByWhiteSpace() throws PigLatinException {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithCompositeWords() throws PigLatinException {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithCompositeAndWhiteSpaceWords() throws PigLatinException {
		String inputPhrase = "well-being hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay ellohay orldway",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithPunctuations() throws PigLatinException {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithUppercaseWords() throws PigLatinException {
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEYAY",translator.translate());
		// La corretta traduzione di APPLE � APPLEYAY e non APPLEAY
	}
	
	@Test
	public void testTranslationPhraseWithTitlecaseWords() throws PigLatinException {
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay",translator.translate());
	}
	
	@Test(expected = PigLatinException.class)
	public void testInvalidTranslationPhrase() throws PigLatinException {
		String inputPhrase = "helLo";
		Translator translator = new Translator(inputPhrase);
		translator.translate();
		
	}
}
