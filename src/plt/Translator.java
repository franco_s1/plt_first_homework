package plt;

import java.util.ArrayList;
import java.util.Arrays;

public class Translator {

	public static final String NIL = "nil";
	private boolean composite = false;
	private String phrase;
	private String finalphrase = "";
	private String startp = "";
	private String endp = "";
	private static final int LOWERCASE = 0;
	private static final int TITLECASE = 1;
	private static final int UPPERCASE = 2;
	private int upper;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() throws PigLatinException {
		if (phrase.isEmpty()) {
			return NIL;
		}
		String[] words = splitPhraseBySymbol(" ", phrase);
		for (String word : words) {
			composite = false;
			String[] compositewords = splitPhraseBySymbol("-", word);
			int j = 1;
			for (String cword : compositewords) {
				upper = typeOf(cword);
				cword = wordWithNoPunct(cword);
				if (startWithVowel(cword)) {
					String app = endWith(cword);
					uppercaseOutput(cword, app);
				} else {
					int i = 1;
					while (!startWithVowel(cword.substring(i))) {
						i++;
					}
					titlecaseOutput(cword, i);
				}
				if (composite && j < compositewords.length) {
					finalphrase = finalphrase.concat("-");
				} else {
					finalphrase = finalphrase.concat(" ");
				}
				j++;
			}
		}
		return finalphrase.substring(0, finalphrase.length() - 1);

	}

	private void titlecaseOutput(String word, int i) {
		if (upper == TITLECASE) {
			finalphrase = finalphrase.concat(startp.concat(word.substring(i, i + 1).toUpperCase()))
					.concat(word.substring(i + 1)).concat(word.substring(0, i).toLowerCase()).concat("ay").concat(endp);
		} else {
			finalphrase = finalphrase.concat(startp.concat(word).substring(i)).concat(word.substring(0, i)).concat("ay")
					.concat(endp);
		}
	}

	private void uppercaseOutput(String word, String app) {
		if (upper == UPPERCASE) {
			finalphrase = finalphrase.concat(startp.concat(word)).concat(app).toUpperCase().concat(endp);
		} else {
			finalphrase = finalphrase.concat(startp.concat(word)).concat(app).concat(endp);
		}
	}

	private String[] splitPhraseBySymbol(String symbol, String phrase) {
		String[] listwords = phrase.split("  ");
		if (phrase.contains(symbol)) {
			composite = true;
			return phrase.split(symbol);
		}
		return listwords;
	}

	private String endWith(String word) {
		word = word.toLowerCase();
		if (word.endsWith("y")) {
			return "nay";
		} else if (endWithVowel(word)) {
			return "yay";
		}
		return "ay";

	}

	private int typeOf(String word) throws PigLatinException {
		if (word.equals(word.toUpperCase())) {
			return UPPERCASE;
		} else if (word.substring(0, 1).equals(word.substring(0, 1).toUpperCase())) {
			return TITLECASE;
		} else if (word.equals(word.toLowerCase())) {
			return LOWERCASE;
		} else {
			throw new PigLatinException();
		}
	}

	private String wordWithNoPunct(String word) {

		if (word.substring(0, 1).equalsIgnoreCase(".") || word.substring(0, 1).equalsIgnoreCase(",")
				|| word.substring(0, 1).equalsIgnoreCase(";") || word.substring(0, 1).equalsIgnoreCase(":")
				|| word.substring(0, 1).equalsIgnoreCase("?") || word.substring(0, 1).equalsIgnoreCase("!")
				|| word.substring(0, 1).equalsIgnoreCase("'") || word.substring(0, 1).equalsIgnoreCase("(")
				|| word.substring(0, 1).equalsIgnoreCase(")")) {
			startp = word.substring(0, 1);
			word = word.substring(1);
		}
		if (word.substring(word.length() - 1, word.length()).equalsIgnoreCase(".")
				|| word.substring(word.length() - 1, word.length()).equalsIgnoreCase(",")
				|| word.substring(word.length() - 1, word.length()).equalsIgnoreCase(";")
				|| word.substring(word.length() - 1, word.length()).equalsIgnoreCase(":")
				|| word.substring(word.length() - 1, word.length()).equalsIgnoreCase("?")
				|| word.substring(word.length() - 1, word.length()).equalsIgnoreCase("!")
				|| word.substring(word.length() - 1, word.length()).equalsIgnoreCase("'")
				|| word.substring(word.length() - 1, word.length()).equalsIgnoreCase("(")
				|| word.substring(word.length() - 1, word.length()).equalsIgnoreCase(")")) {
			endp = word.substring(word.length() - 1, word.length());
			word = word.substring(0, word.length() - 1);
		}
		return word;
	}

	private boolean startWithVowel(String word) {
		word = word.toLowerCase();
		return (word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o")
				|| word.startsWith("u"));
	}

	private boolean endWithVowel(String word) {
		word = word.toLowerCase();
		return (word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o")
				|| word.endsWith("u"));
	}

}
